#Tee ohjelma, joka kysyy käyttäjältä 2 kokonaislukua ja tulostaa niistä suuremman. Jos luvut ovat yhtä suuria, antaa ohjelma siitä viestin “Yhtä suuria”.
luku1 = int(input("Anna luku: "))
luku2 = int(input("Anna toinen luku: "))

if luku1 > luku2:
    print(f"Luku {luku1} on suurempi")
if luku1 < luku2:
    print(f"Luku {luku2} on suurempi")

if luku1 == luku2:
    print("Luvut ovat yhtä suuret")
