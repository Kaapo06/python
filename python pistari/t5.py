#Tee ohjelma, joka pyytää käyttäjältä tämän iän ja kertoo, voiko käyttäjä tulla clubille vai eikö voi. Clubin ikäraja on 18–80 vuotiaat.
ikä = int(input("Kerro ikäsi: "))

if ikä < 18 or ikä > 80:
    print("Et pääse")
if ikä >= 18 and ikä <= 80:
    print("Pääset")
