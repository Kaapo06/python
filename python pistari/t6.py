#Tee ohjelma, joka kysyy käyttäjältä pistemäärän. Sitten ohjelma tulostaa pistemäärää vastaavan arvosanan. Arvosanan määräytyminen:
arvosana = int(input("Anna arvosanasi: "))

if arvosana <= 50:
    print("0")

if arvosana >= 51 and arvosana <= 70:
    print("1")

if arvosana >= 71 and arvosana <= 90:
    print("2")

if arvosana >= 91 and arvosana <= 100:
    print("3")